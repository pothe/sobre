# CursApp (Cursos por WhatsApp)

**Se impartirán cursos por WhatsApp**

La idea es impartir cursos por el mismo WhatsApp, para que así se pueda crear una comunidad interesada en un mismo tema de interés como pueden ser:
* WordPress
* PHP
* JavaScript
* CSS
* Node.js
* Desarrollo Web
* Git
* Etc.

Creando minicursos interesantes pero potenciales como los de PopUp o de Marca Personal.

Y si se puede crear una página web donde se anuncien los cursos y se unan las personas.
